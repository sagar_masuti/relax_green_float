!-----------------------------------------------------------------------
! Copyright 2007, 2008, 2009 Sylvain Barbot
!
! This file is part of RELAX
!
! RELAX is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! RELAX is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with RELAX.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "include.f90"

MODULE export
   USE types
!  USE elastic3d
!  USE viscoelastic3d
!  USE friction3d

  IMPLICIT NONE

CONTAINS

  !-------------------------------------------------------------------
  ! routine ReportTime
  ! writes the times of exports
  !
  ! sylvain barbot (04/29/09) - original form
  !-------------------------------------------------------------------
  SUBROUTINE reporttime(i,t,repfile)
    INTEGER, INTENT(IN) :: i
    CHARACTER(256), INTENT(IN) :: repfile
    REAL*8, INTENT(IN) :: t

    INTEGER :: iostatus

    IF (0 .eq. i) THEN
       OPEN (UNIT=15,FILE=repfile,IOSTAT=iostatus,FORM="FORMATTED")
    ELSE
       OPEN (UNIT=15,FILE=repfile,POSITION="APPEND",&
            IOSTAT=iostatus,FORM="FORMATTED")
    END IF
    IF (iostatus>0) THEN
       WRITE_DEBUG_INFO
       PRINT '(a)', repfile
       STOP "could not open file for export"
    END IF

    WRITE (15,'(ES11.3E2)') t

    CLOSE(15)

  END SUBROUTINE reporttime

  SUBROUTINE report(i,t,file1,file2,file3,sx1,sx2,repfile)
    INTEGER, INTENT(IN) :: i,sx1,sx2
    CHARACTER(256), INTENT(IN) :: file1,file2,file3,repfile
    REAL*8, INTENT(IN) :: t

    INTEGER :: iostatus, ind1,ind2,ind3

    IF (0 .eq. i) THEN
       OPEN (UNIT=15,FILE=repfile,IOSTAT=iostatus,FORM="FORMATTED")
    ELSE
       OPEN (UNIT=15,FILE=repfile,POSITION="APPEND",&
            IOSTAT=iostatus,FORM="FORMATTED")
    END IF
    IF (iostatus>0) THEN
       WRITE_DEBUG_INFO
       PRINT '(a)', repfile
       STOP "could not open file for export"
    END IF

    ind1=INDEX(file1," ")
    ind2=INDEX(file2," ")
    ind3=INDEX(file3," ")
    WRITE (15,'(I3.3,2I6," ",f13.4," ",a," ",a," ",a)') i,sx1,sx2,t,&
         file1(1:ind1-1),file2(1:ind2-1),file3(1:ind3-1)

    CLOSE(15)

  END SUBROUTINE report

  !------------------------------------------------------------------

  !------------------------------------------------------------------
  !> subroutine exportpoints
  !! sample a vector field at a series of points for export.
  !! each location is attributed a file in which the time evolution
  !! of the vector value is listed in the format:
  !!
  !!                t_0 u(t_0) v(t_0) w(t_0)
  !!                t_1 u(t_1) v(t_1) w(t_1)
  !!                ...
  !!
  !! \author sylvain barbot (11/10/07) - original form
  !------------------------------------------------------------------
  SUBROUTINE exportpoints(c1,c2,c3,sig,sx1,sx2,sx3,dx1,dx2,dx3, &
       opts,ptsname,time,wdir,isnew,x0,y0,rot)
    INTEGER, INTENT(IN) :: sx1,sx2,sx3
#ifdef ALIGN_DATA
    REAL*4, INTENT(IN), DIMENSION(sx1+2,sx2,sx3) :: c1,c2,c3
#else
    REAL*4, INTENT(IN), DIMENSION(sx1,sx2,sx3) :: c1,c2,c3
#endif
    TYPE(TENSOR), INTENT(IN), DIMENSION(sx1,sx2,sx3) :: sig
    TYPE(VECTOR_STRUCT), INTENT(IN), DIMENSION(:) :: opts
    CHARACTER(LEN=4), INTENT(IN), DIMENSION(:) :: ptsname
    REAL*8, INTENT(IN) :: dx1,dx2,dx3,time,x0,y0,rot
    CHARACTER(256), INTENT(IN) :: wdir
    LOGICAL, INTENT(IN) :: isnew

    INTEGER :: i1,i2,i3,n,k
    REAL*8 :: u1,u2,u3,v1,v2,v3,x1,x2,x3,y1,y2,y3
    TYPE(TENSOR) :: lsig
    INTEGER :: i,iostatus
    CHARACTER(256) :: file1,file2

    i=INDEX(wdir," ")
    n=SIZE(ptsname)

    DO k=1,n
       file1=wdir(1:i-1) // "/" // ptsname(k) // ".txt"
       IF (isnew) THEN
          OPEN (UNIT=15,FILE=file1,IOSTAT=iostatus,FORM="FORMATTED")
          WRITE (15,'("#         t         u1         u2         u3        ", &
                    & "s11        s12        s13        s22        s23        s33")')
       ELSE
          OPEN (UNIT=15,FILE=file1,POSITION="APPEND",&
               IOSTAT=iostatus,FORM="FORMATTED")
       END IF
       IF (iostatus>0) STOP "could not open point file for writing"

       x1=opts(k)%v1
       x2=opts(k)%v2
       x3=opts(k)%v3

       CALL shiftedindex(x1,x2,x3,sx1,sx2,sx3,dx1,dx2,dx3,i1,i2,i3)

       u1=c1(i1,i2,i3)
       u2=c2(i1,i2,i3)
       u3=c3(i1,i2,i3)
       lsig=sig(i1,i2,i3)

       ! change from computational reference frame to user reference system
       y1=x1;v1=u1
       y2=x2;v2=u2
       y3=x3;v3=u3

       CALL rotation(y1,y2,-rot)
       y1=y1+x0
       y2=y2+y0
       CALL rotation(v1,v2,-rot)

       x1=x1+x0
       x2=x2+y0

       WRITE (15,'(13ES11.3E2)') time,v1,v2,v3, &
                                 lsig%s11,lsig%s12,lsig%s13, &
                                 lsig%s22,lsig%s23,lsig%s33
       CLOSE(15)

       ! output in rotated coordinates system
       IF (0._8.NE.rot) THEN
          file2=wdir(1:i-1) // "/" // ptsname(k) // ".c.txt"
          IF (isnew) THEN
             OPEN (UNIT=16,FILE=file2,IOSTAT=iostatus,FORM="FORMATTED")
             WRITE (16,'("#         t         u1         u2         u3")')
          ELSE
             OPEN (UNIT=16,FILE=file2,POSITION="APPEND",&
                   IOSTAT=iostatus,FORM="FORMATTED")
          END IF
          IF (iostatus>0) STOP "could not open point file for writing"

          WRITE (16,'(7ES11.3E2)') time,u1,u2,u3
          CLOSE(16)
       END IF

    END DO

  CONTAINS

    !------------------------------------------------------------------
    ! subroutine Rotation
    ! rotates a point coordinate into the computational reference
    ! system.
    ! 
    ! sylvain barbot (04/16/09) - original form
    !------------------------------------------------------------------
    SUBROUTINE rotation(x,y,rot)
      REAL*8, INTENT(INOUT) :: x,y
      REAL*8, INTENT(IN) :: rot

      REAL*8 :: alpha,xx,yy
      REAL*8, PARAMETER :: DEG2RAD = 0.01745329251994329547437168059786927_8


      alpha=rot*DEG2RAD
      xx=x
      yy=y

      x=+xx*cos(alpha)+yy*sin(alpha)
      y=-xx*sin(alpha)+yy*cos(alpha)

    END SUBROUTINE rotation

  END SUBROUTINE exportpoints

  !---------------------------------------------------------------------
  !> subroutine exportoptsdat
  !! export the coordinates and name of the observation points (often
  !! coordinates of GPS instruments or such) for display with GMT in the
  !! ASCII format. The file contains a list of x1,x2,x3 coordinates and
  !! a 4-character name string.
  !!
  !! input variables
  !! @param n          - number of observation points
  !! @param opts       - coordinates of observation points
  !! @param ptsname    - name of obs. points
  !! @param filename   - output file (example: wdir/opts.xy)
  !!
  !! \author sylvain barbot (08/10/11) - original form
  !---------------------------------------------------------------------
  SUBROUTINE exportoptsdat(n,opts,ptsname,filename)
    INTEGER, INTENT(IN) :: n
    TYPE(VECTOR_STRUCT), DIMENSION(n) :: opts
    CHARACTER(LEN=4), DIMENSION(n) :: ptsname
    CHARACTER(256) :: filename

    INTEGER :: k,iostatus

    IF (n.LE.0) RETURN

    OPEN (UNIT=15,FILE=filename,IOSTAT=iostatus,FORM="FORMATTED")
    IF (iostatus>0) STOP "could not open .xy file to export observation points"
    WRITE (15,'("# x1(north) x2(east) x3(down) name(4-char)")')
    DO k=1,n
       WRITE (15,'(3ES11.4E1,X,a)') opts(k)%v1,opts(k)%v2,opts(k)%v3,ptsname(k)
    END DO
    CLOSE(15)
    
  END SUBROUTINE exportoptsdat

  !------------------------------------------------------------
  !> function fix
  !! returns the closest integer scalar
  !
  ! sylvain barbot (08/25/07) - original form
  !------------------------------------------------------------
  INTEGER FUNCTION fix(number)
    REAL*8, INTENT(IN) :: number

    INTEGER :: c,f
    f=FLOOR(number)
    c=CEILING(number)

    IF ((number-f) .gt. 0.5_8) THEN
       fix=c
    ELSE
       fix=f
    END IF

  END FUNCTION fix

  !----------------------------------------------------------------------
  !> subroutine ShiftedIndex
  !! returns the integer index corresponding to the specified coordinates
  !! assuming the data are ordered following fftshift. input coordinates
  !! are assumed bounded -sx/2 <= x <= sx/2-1. out of bound input
  !! purposefully triggers a fatal error. in the x3 direction, coordinates
  !! are assumed bounded by 0 <= x3 <= (sx3-1)*dx3
  !!
  !! CALLED BY:
  !!   monitorfield/sample
  !!
  !! \author sylvain barbot (07/31/07) - original form
  !----------------------------------------------------------------------
  SUBROUTINE shiftedindex(x1,x2,x3,sx1,sx2,sx3,dx1,dx2,dx3,i1,i2,i3)
    REAL*8, INTENT(IN) :: x1,x2,x3,dx1,dx2,dx3
    INTEGER, INTENT(IN) :: sx1,sx2,sx3
    INTEGER, INTENT(OUT) :: i1,i2,i3

    IF (x1 .gt.  DBLE(sx1/2-1)*dx1) THEN
       WRITE_DEBUG_INFO
       WRITE (0,'("x1=",ES9.2E2,"; boundary at x1=",ES9.2E2)') x1, DBLE(sx1/2)*dx1
       STOP "ShiftedIndex:invalid x1 coordinates (x1 too large)"
    END IF
    IF (x1 .lt. -DBLE(sx1/2)*dx1  ) THEN
       WRITE_DEBUG_INFO
       WRITE (0,'("x1=",ES9.2E2,"; boundary at x1=",ES9.2E2)') x1, -DBLE(sx1/2)*dx1
       STOP "ShiftedIndex:coordinates out of range (-x1 too large)"
    END IF
    IF (x2 .gt.  DBLE(sx2/2-1)*dx2) THEN
       WRITE_DEBUG_INFO
       WRITE (0,'("x2=",ES9.2E2,"; boundary at x2=",ES9.2E2)') x2, DBLE(sx2/2)*dx2
       STOP "ShiftedIndex:invalid x2 coordinates (x2 too large)"
    END IF
    IF (x2 .lt. -DBLE(sx2/2)*dx2  ) THEN
       WRITE_DEBUG_INFO
       WRITE (0,'("x2=",ES9.2E2,"; boundary at x2=",ES9.2E2)') x2, -DBLE(sx2/2)*dx2
       STOP "ShiftedIndex:coordinates out of range (-x2 too large)"
    END IF
    IF (x3 .gt.  DBLE(sx3-1)*dx3) THEN
       WRITE_DEBUG_INFO
       STOP "ShiftedIndex:invalid x3 coordinates (x3 too large)"
    END IF
    IF (x3 .lt.  0              )   THEN
       WRITE (0,'("x3=",ES9.2E2)') x3
       STOP "ShiftedIndex:coordinates out of range (x3 negative)"
    END IF

    i1=MOD(sx1+fix(x1/dx1),sx1)+1
    i2=MOD(sx2+fix(x2/dx2),sx2)+1
    i3=fix(x3/dx3)+1
 
  END SUBROUTINE shiftedindex

END MODULE
