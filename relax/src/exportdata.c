
#include "config.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void exportdata_ (float *fd1, float *fd2, float *fd3, int iSx1, int iSx2, int iSx3, char *cFilename)
{
    int i, j, k, iIndex ;
	unsigned long int len ; 
   	FILE *fp ;
	char *filename ;

	len = strlen(cFilename) ;
	filename = (char *) calloc(len,1) ;
	if (filename == NULL)
	{
		return ;
	}
	strncpy(filename, cFilename, len-2) ;
	
	/* Open the file in append mode  */
	if (NULL == (fp = fopen(filename, "r"))) 
	{
		fp = fopen (filename, "w") ;
		fprintf (fp, "# fd1\t\t\tfd2\t\t\tfd3\n") ;
		for (i = 0; i < (iSx3); i++)
		{
			for (j = 0 ; j < (iSx2); j++)
			{
				for (k = 0; k < (iSx1 + 2); k++)
				{
					iIndex = (i * iSx2 + j) * (iSx1 + 2)  + k ;
					fprintf(fp, "%12.5e %12.5e %12.5e\n", fd1[iIndex], fd2[iIndex], fd3[iIndex]) ; 
				}   
			}
		}
	}

	/* close the file */
	fclose (fp) ;
	
}
