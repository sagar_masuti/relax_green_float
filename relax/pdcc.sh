
module load cuda50/toolkit/5.0.35 
#module load cuda65/toolkit/6.5.14    
module load proj/4.8.0 
./waf


if ! [ $? = 0 ]; then
	rm -rf build
	#./waf configure --use-fftw --fftw-dir=/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3 --use-cuda --cuda-dir=/cm/shared/apps/cuda50/toolkit/current/ --use-papi --papi-dir=/cm/shared/apps/papi-5.2.0
	./waf configure --use-fftw --fftw-dir=/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3 --use-cuda --cuda-dir=/cm/shared/apps/cuda50/toolkit/current --use-papi --papi-dir=/cm/shared/apps/papi-5.2.0
#	./waf configure --use-fftw --fftw-dir=/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3 --use-papi --papi-dir=/cm/shared/apps/papi-5.2.0
	./waf
fi 

export PATH=$PATH:$PWD/build

sbatch examples/tutorials/run2.sh
