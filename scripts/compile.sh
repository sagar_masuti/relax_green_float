#!/bin/bash

module unload gcc/4.8.1
module unload gcc/4.7.0
module load fftw3/openmpi/gcc/64/3.3.3
module load intel/mkl/64/11.0/2013.5.192
module load cuda50/toolkit/5.0.35

if [[ $1 == "-w" ]]; then
	paths=" --use-fftw"
	if [[ $HOSTNAME == "pdccmc" ]]; then
		paths+=" --fftw-dir=/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3"
	fi
elif [[ $1 == "-c" ]]; then
	paths=" --use-fftw --use-cuda"
	if [[ $HOSTNAME == "pdccmc" ]]; then
		paths+=" --fftw-dir=/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3"
	fi
fi

#paths+=" --use-papi --papi-incdir=/usr/local/include --papi-libdir=/usr/local/lib"
if [[ $HOSTNAME == "pdccmc" ]]; then
	paths+=" --use-papi --papi-dir=/cm/shared/apps/papi-5.2.0"
fi

RELAX_PATH=~/workspace/relax_green/relax
pushd $RELAX_PATH

rm -rf ./build
echo $paths
echo ./waf configure $paths
./waf configure $paths

if [[ $1 == "-c" ]]; then
	sed -i '6i #define USING_CUDA 1' $RELAX_PATH/build/config.h
fi

./waf

popd
