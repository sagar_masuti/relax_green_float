#!/bin/bash

RELAX_DATA_PATH=$PWD/../data

mkdir -p $RELAX_DATA_PATH

RELAX_WSCRIPT_PATH=$PWD/../relax

pushd $RELAX_WSCRIPT_PATH
export PATH=$PATH:$PWD/build

#for reg in 16 32 48 54 60 64 72 84 96
for reg in 16 32
do
	sed -i "s/--maxrregcount=[0-9]*/--maxrregcount=$reg/" $RELAX_WSCRIPT_PATH/wscript

	rm -rf build/

	./waf configure --use-fftw --use-cuda --use-papi
	./waf

	./examples/tutorials/run2.sh > $RELAX_DATA_PATH/maxreg_$reg.txt
done
popd
