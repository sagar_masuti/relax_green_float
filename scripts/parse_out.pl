#!/usr/bin/perl
###################################################################################
# This script is used to extract statistics from a single execution of the VTR flow
#
# Usage:
#	parse_vtr_flow.pl <parse_path> <parse_config_file>
#
# Parameters:
#	parse_path: Directory path that contains the files that will be 
#					parsed (vpr.out, odin.out, etc).
#	parse_config_file: Path to the Parse Configuration File
###################################################################################

use strict;
use Cwd;
use File::Spec;
use Getopt::Std;

getopts('h:d:c:');
#{
#	die "Usage: parse_out.pl -h -d -c\n -h = sql header\n -d = sql data,\n -c = csv data";
#}

our ($opt_h, $opt_d, $opt_c);
my $header = $opt_h || 0;
my $sql = $opt_d || 0;
my $csv  = $opt_c || 0;

sub expand_user_path;

my $parse_path = expand_user_path(shift);
my $parse_config_file = expand_user_path(shift);

if (! -r $parse_config_file)
{
	die "Cannot find parse file ($parse_config_file)\n";
}

open (PARSE_FILE, $parse_config_file);
my @parse_lines = <PARSE_FILE>;
close (PARSE_FILE);

my @parse_data;
my $file_to_parse;
foreach my $line (@parse_lines)
{
	chomp($line);
	
	# Ignore comments
	if ( $line =~ /^\s*#.*$/ or $line =~ /^\s*$/ ) { next; }
	
	my @name_file_regexp = split( /;/, $line, 5 );
	push(@parse_data, [@name_file_regexp]);	
}

if ( $header ) 
{
	print "CREATE TABLE data (";
	for my $parse_entry (@parse_data)
	{
		print "\"" . @$parse_entry[0] . "\" " . @$parse_entry[3] . ", ";
	}
	print "timestamp real, PRIMARY KEY (";
	my $index=0;
	for my $parse_entry (@parse_data)
	{
		if (@$parse_entry[4] != "0" && $index > 0 )
		{
			print ",";
		}
		$index++;

		if (@$parse_entry[4] == "1")
		{
			print "\"" . @$parse_entry[0] . "\"";
		}

	}
	print "));\n";
}

if ( $sql ) 
{
	print "INSERT OR REPLACE INTO data VALUES (";
}

for my $parse_entry (@parse_data)
{
	my $file_to_parse = @$parse_entry[1];
	if (not -r "${parse_path}/${file_to_parse}")
	{
		die "Cannot open file to parse (${parse_path}/${file_to_parse})";
	}
	undef $/;
	open (DATA_FILE, "<$parse_path/$file_to_parse");
	my $parse_file_lines = <DATA_FILE>;
	close(DATA_FILE);
	$/ = "\n";
	
	my $regexp = @$parse_entry[2];
	if ($parse_file_lines =~ m/$regexp/g)
	{
		my $str = $1;
		$str =~ s/,//g;
		$str =~ s/ //g;
		if ( $sql ) {
			print "'" . $str . "',";
		} else {
			print $str . ",";
		}
	} else {
		if ( $sql ) {
			print "'-1',";
		} else {
			print "#";
		}
	}

}
if ( $sql ) {
	print " '" . time . "'); \n";
} else {
	print "\n";
}

sub expand_user_path
{
	my $str = shift;	
	$str =~ s/^~\//$ENV{"HOME"}\//;
	return $str;
}
