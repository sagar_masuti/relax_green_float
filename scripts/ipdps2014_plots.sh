#!/bin/zsh

cp all_data_combined.csv all_data_combined.csv.bak
if [[ $1 -eq 1 ]]; then
else 
	./gather_results.sh > all_data_combined.csv
fi

#FINAL RESULT: best cuda total vs fftw/mkl total
cat all_data_combined.csv | grep "512512256"  | grep "1,mkl" | sed "s/mkl/mkl1/" | cut -d"," -f 3,12 > final_green.csv
cat all_data_combined.csv | grep "512512256"  | grep "16,fftw\|16,mkl\|1,cuda" | sed "s/mkl/mkl16/" | sed "s/fftw/fftw16/" | cut -d"," -f 3,12 >> final_green.csv

#CPU VS> INDIVIDUAL FUNCTIONS BREAKDOWN: individual function runtimes
cat all_data_combined.csv | grep "#\|mkl" | grep "#\|,1,"  | sed "s/#//g" | tr '[:upper:]' '[:lower:]' | awk -F, '{ print $1 "," $2 "," $3 "," $4 "," $8 "," $6 "," $5 "," $7 "," $9 "," $11 "," $13 }' > single_thread.csv

cat all_data_combined.csv | grep "#\|mkl" | grep "#\|512512256" | sed "s/#//g" | tr '[:upper:]' '[:lower:]' | awk -F, '{ print $1 "," $2 "," $3 "," $4 "," $8 "," $6 "," $5 "," $7 "," $9 "," $11 "," $13 }' > multi_thread_mkl.csv
cat all_data_combined.csv | grep "#\|fftw" | grep "#\|512512256" | sed "s/#//g" | tr '[:upper:]' '[:lower:]' | awk -F, '{ print $1 "," $2 "," $3 "," $4 "," $8 "," $6 "," $5 "," $7 "," $9 "," $11 "," $13 }' > multi_thread_fftw.csv

#CPU THREADS TOTAL: CPU-based parallel runtimes
cat all_data_combined.csv | grep "mkl" | grep "512512256" | cut -d, -f 2,12 > threads_cpu_mkl.csv
cat all_data_combined.csv | grep "fftw" | grep "512512256" | cut -d, -f 2,12 > threads_cpu_fftw.csv

#CPU VS. PROBLEM SIZE
cat all_data_combined.csv | grep "mkl" | grep ",16," | cut -d, -f 1,12 > problem_size_cpu_mkl.csv
cat all_data_combined.csv | grep "fftw" | grep ",16," | cut -d, -f 1,12 > problem_size_cpu_fftw.csv

#CUDA VS. PROBLEM SIZE: 
cat all_data_combined.csv | grep "cuda" | cut -d"," -f 1,12 > problem_size_green.csv

#CUDA VS. INDIVIDUAL FUNCTIONS BREAKDOWN:
cat all_data_combined.csv | grep "#\|cuda" | grep "#\|512512256" | sed "s/#//g" | tr '[:upper:]' '[:lower:]' | awk -F, '{ print $1 "," $2 "," $3 "," $4 "," $8 "," $6 "," $5 "," $7 "," $9 "," $11 "," $13 }' > cuda.csv

cat cuda_k20.csv | grep "#\|cuda" | grep "#\|256256256" | sed "s/#//g" | sed "s/cuda/k20/g" | tr '[:upper:]' '[:lower:]' | awk -F, '{ print $1 "," $2 "," $3 "," $4 "," $8 "," $6 "," $5 "," $7 "," $9 "," $11 "," $13 }' > cuda_k20_i.csv
cat cuda_gtx680.csv | grep "cuda" | grep "256256256" | sed "s/cuda/gtx680/g" | tr '[:upper:]' '[:lower:]' | awk -F, '{ print $1 "," $2 "," $3 "," $4 "," $8 "," $6 "," $5 "," $7 "," $9 "," $11 "," $13 }' > cuda_gtx680_i.csv
cat cuda_c2075.csv | grep "cuda" | grep "256256256" | sed "s/cuda/c2075/g" | tr '[:upper:]' '[:lower:]' | awk -F, '{ print $1 "," $2 "," $3 "," $4 "," $8 "," $6 "," $5 "," $7 "," $9 "," $11 "," $13 }' > cuda_c2075_i.csv

#CUDA VS. DEVICE
cat all_data_combined.csv | grep "512512256" | grep "cuda" | cut -d, -f 12 > cuda_${HOSTNAME}.csv

if [[ $1 -eq 1 ]]; then
	cp *.csv ~/Dropbox/Research/papers/2013/relax_gpu_ipdps2014/data
fi
